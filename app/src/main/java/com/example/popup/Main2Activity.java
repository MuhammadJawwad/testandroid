package com.example.popup;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Main2Activity extends AppCompatActivity {

    private Button btnNotificationActions, btnHeadsUpNotification, btnBigTextStyle, btnBigPictureStyle, btnInboxStyle, btnMessageStyle;
    NotificationCompat.Builder buildNotification;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        btnNotificationActions = findViewById(R.id.actionNotifi);
        btnNotificationActions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                actionNotification();
            }
        });
        btnHeadsUpNotification = findViewById(R.id.headsNotifi);
        btnHeadsUpNotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                headsUpNotification();
            }
        });
        btnBigTextStyle = findViewById(R.id.bigTextStyleNotifi);
        btnBigPictureStyle = findViewById(R.id.pictureNotifi);
        btnInboxStyle = findViewById(R.id.inboxNotifi);
        btnMessageStyle = findViewById(R.id.messageNotifi);
    }

    private void headsUpNotification(){
        int notificationID = 2;
        buildNotification = new NotificationCompat.Builder(this);
        buildNotification.setDefaults(NotificationCompat.DEFAULT_ALL).setPriority(NotificationCompat.PRIORITY_MAX);
        buildNotification.setContentTitle("HeadsUp Notification").setSmallIcon(R.drawable.image2)
                .setAutoCancel(true);
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.google.com"));
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 1,intent, 0);
        Intent intentBtn = new Intent(getBaseContext(), PlayActivity.class);
        PendingIntent pendingBtnIntent = PendingIntent.getActivity(this,0, intentBtn, 0);
        Intent btnIntent = new Intent(getBaseContext(),NotificationReciever.class);
        btnIntent.putExtra("notificationId",notificationID);
        PendingIntent dismissIntent = PendingIntent.getActivity(this,0,btnIntent,PendingIntent.FLAG_ONE_SHOT);

        buildNotification.addAction(android.R.drawable.ic_media_previous, "Pause",pendingIntent);
        buildNotification.addAction(android.R.drawable.ic_media_play, "Play", pendingBtnIntent);
        buildNotification.addAction(android.R.drawable.ic_menu_close_clear_cancel,"CLose",dismissIntent);
        buildNotification(notificationID);
    }

    private void actionNotification(){
        int notificationID = 1;
        buildNotification = new NotificationCompat.Builder(this);
        buildNotification.setTicker("Notification Recieved");
        buildNotification.setSmallIcon(R.drawable.image2);
        buildNotification.setColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));
        buildNotification.setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.image2));
        buildNotification.setContentTitle("Notification Actions");
        buildNotification.setContentText("Tap View to launch our website");
        buildNotification.setAutoCancel(true);

        PendingIntent launchIntent = getLaunchIntent(notificationID, getBaseContext());
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.journaldev.com"));

        PendingIntent pendingIntent = PendingIntent.getActivity(getBaseContext(),0, intent, 0);
        Intent btnIntent = new Intent(getBaseContext(), NotificationReciever.class);
        PendingIntent dismissIntent = PendingIntent.getActivity(getBaseContext(), 0 , btnIntent, 0);

        buildNotification.setContentIntent(launchIntent);
        buildNotification.addAction(android.R.drawable.arrow_down_float, "Play", pendingIntent);
        buildNotification.addAction(android.R.drawable.btn_radio,"Radio", dismissIntent);


        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        notificationManager.notify(notificationID, buildNotification.build());
    }
    public PendingIntent getLaunchIntent(int notificationID, Context context)
    {
        Intent intent = new Intent(context, Main2Activity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.putExtra("notificationID", notificationID);
        return PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);
    }
    private void clearNotification() {
        int notificationID = getIntent().getIntExtra("notificationID", 0);

        NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        manager.cancel(notificationID);
    }
    private void buildNotification(int notificationId){
        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        notificationManager.notify(notificationId, buildNotification.build());
    }
}
