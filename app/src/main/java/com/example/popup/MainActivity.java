package com.example.popup;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.PopupMenu;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements PopupMenu.OnMenuItemClickListener {

    PopupMenu popupMenu;
    int id = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btnNext = findViewById(R.id.btnNext);
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, Main2Activity.class));
            }
        });

        Button btnNotification = findViewById(R.id.notification);
        btnNotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                final NotificationCompat.Builder notifyBuilder = new NotificationCompat.Builder(MainActivity.this);
                notifyBuilder.setContentTitle("Downloading...")
                        .setContentText("Downloading file....")
                        .setSmallIcon(R.drawable.image2).setTicker("New Notification Arrived.");
                new Thread(
                        new Runnable() {
                    @Override
                    public void run() {
                        int incre;
                        for (incre = 0; incre <= 100; incre += 10) {
                            notifyBuilder.setProgress(100, incre, false);
                            notificationManager.notify(id, notifyBuilder.build());
                            try {
                                Thread.sleep(1 * 1000);
                            } catch (InterruptedException e) {
                                Log.d("TAG", "Sleep Failure");
                            }
                        }
                        notifyBuilder.setContentTitle("Download Complete").setContentText("").setProgress(0, 0, false);
                        notificationManager.notify(id, notifyBuilder.build());
                    }
                }
                ).start();
            }
        });

        Button btnPopUp = findViewById(R.id.btnPopUp);
        btnPopUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPopup(v);
                showNotification();
            }
        });
    }
    public void showNotification()
    {
        NotificationCompat.Builder notifyBuilder = new NotificationCompat.Builder(MainActivity.this)
                .setSmallIcon(R.drawable.image2).setContentTitle("PopUp Pressed");
        Intent intent = new Intent(MainActivity.this, MainActivity.class);
        PendingIntent pIntent = PendingIntent.getActivity(MainActivity.this, 0, intent, 0);
        notifyBuilder.setContentIntent(pIntent);
        Intent btnIntent = new Intent(getApplicationContext(), Main2Activity.class);
        PendingIntent pendingIntent =  PendingIntent.getBroadcast(getApplicationContext(),0,btnIntent,0);
        notifyBuilder.setContentIntent(pendingIntent).addAction(android.R.drawable.btn_radio, "Radio", pendingIntent);
        Intent btnIntent2 = new Intent(getApplicationContext(), NotificationReciever.class);
        PendingIntent pendingIntent1 = PendingIntent.getBroadcast(getApplicationContext(), 0, btnIntent2, 0);
        notifyBuilder.setContentIntent(pendingIntent1).addAction(android.R.drawable.ic_media_play, "Play", pendingIntent1);

        int notifyId = 99;
        NotificationManager notifyManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        notifyManager.notify(notifyId, notifyBuilder.build());
    }
    public void showPopup(View v){
        popupMenu = new PopupMenu(this, v);
        MenuInflater inflater = popupMenu.getMenuInflater();
        inflater.inflate(R.menu.menu, popupMenu.getMenu());
        popupMenu.show();
    }

    @Override
    public boolean onMenuItemClick(MenuItem item)
    {
        int id = item.getItemId();
        if (id == R.id.search_item){

            showNotification();
        } else if (id == R.id.copy_item){

        } return true;
    }
}
